### Docker set up
```bash
# build project
docker compose build

# deploy project
docker-compose up -d

# check deployed port and container list
docker compose ps
```
