## DASHBOARD API
 **Dashboard** Web app which is one of CMS. This app can view the master data by register new account.

### Requirements, tools, languages and other
- [PHP v7.3.28](https://www.php.net/)
- [laravel v8.6.10](https://laravel.com/)
- [Docker](https://docs.docker.com/)

****

### Manual Running steps without docker
1. Cloning Project
```bash
# clone project
git clone https://gitlab.com/khaingmintunn/dashboard.git

# redirect to backend folder
cd dashboard
cd dashboard-api
```

2. Modify env config
- create `.env` file and copy code from `.env.example` in root redictory.
- modify database name, user and password for your local.

3. Open command prompt or terminal
```bash
# migration (create tables)
php artisan migrate

# seeding (insert data to table)
php artisan db:seed

# run dashboard server
php artisan serve

# run dashboard server with specific port
php artisan server --port=8080

# run dashboard server with specific domain and port
php artisan serve --host=0.0.0.0 --port=8080
```
**Backend server is up on http://localhost:8080**
