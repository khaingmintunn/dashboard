<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// signup
Route::post('/signup', [UserController::class, 'signup']);
// user login
Route::post('/login', [UserController::class, 'login']);
// route group for authenticated user
Route::group(['middleware' => ['auth:sanctum']], function () {
    // User
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/profile', [UserController::class, 'show']);
    Route::put('/profile', [UserController::class, 'update']);
    Route::post('/logout', [UserController::class, 'logout']);
    // Master
    Route::get('/masters', [MasterController::class, 'index']);
    Route::get('/masters/{id}', [MasterController::class, 'show']);
});
