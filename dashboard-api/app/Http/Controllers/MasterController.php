<?php

namespace App\Http\Controllers;

use App\Models\Master;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    /**
     * Retrive Master Information List
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masters = Master::all();
        return response()->json([
            "success" => true,
            "masters" => $masters
        ]);
    }

    /**
     * Retrieve Master Information by ID
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $master = Master::find($id);
        return response()->json([
            "success" => true,
            "master" => $master
        ]);
    }
}
