<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json([
            'success' => true,
            'status' => 200,
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        $field = $request->validate([
            'name' => 'required|string|min:4|max:30',
            'email' => 'required|string|unique:users,email|email',
            'password' => 'required|string|min:8|max:30'
        ]);
        $user = User::create([
            'name' => $field['name'],
            'email' => $field['email'],
            'password' => Hash::make($field['password']),
        ]);

        return response()->json([
            'success' => true,
            'status' => 200,
            'user' => $user
        ]);
    }

    /**
     * Display the profile.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();

        return response()->json([
            'success' => true,
            'status' => 200,
            'user' => $user
        ]);
    }

    /**
     * Update the profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $field = $request->validate([
            'name' => 'string|min:4|max:30',
            'email' => 'string|unique:users,email|email',
            'password' => 'string|min:8|max:30'
        ]);
        $id = auth()->user()->id;
        $user = User::find($id);
        if (isset($field['password'])) {
            $field['password'] = Hash::make($field['password']);
        }
        $user->update($field);

        return response()->json([
            'success' => true,
            'status' => 200,
            'user' => $user
        ]);
    }

    /**
     * Login the specified account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $field = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:8|max:30'
        ]);
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Invalid user credential.'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json([
            'success' => true,
            'status' => 200,
            'user' => $user,
            'token' => 'Bearer ' . $token
        ]);
    }

    /**
     * logout the specified account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $data = auth()->user()->tokens()->delete();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Logging out successfully.'
        ]);
    }
}
