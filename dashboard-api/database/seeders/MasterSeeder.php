<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('masters')->insert([
            [
                'external_id' => 1301,
                'name' => 'LOUIS VUITTON',
                'is_active' => 1,
                'list_order' => 1,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1302,
                'name' => 'HERMES',
                'is_active' => 1,
                'list_order' => 3,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1303,
                'name' => 'CHANEL',
                'is_active' => 1,
                'list_order' => 2,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1304,
                'name' => 'GUCCI',
                'is_active' => 1,
                'list_order' => 4,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1305,
                'name' => 'COACH',
                'is_active' => 1,
                'list_order' => 47,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1306,
                'name' => 'Christian Dior',
                'is_active' => 0,
                'list_order' => 43,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1307,
                'name' => 'BVLGARI',
                'is_active' => 1,
                'list_order' => 10,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1308,
                'name' => 'DOLCE・・ABBANA',
                'is_active' => 1,
                'list_order' => 55,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1309,
                'name' => 'ROLEX',
                'is_active' => 1,
                'list_order' => 6,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1310,
                'name' => 'ARMANI',
                'is_active' => 1,
                'list_order' => 19,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1311,
                'name' => 'ANTEPRIMA',
                'is_active' => 1,
                'list_order' => 17,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1312,
                'name' => 'UNTITLED',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1313,
                'name' => 'OMEGA',
                'is_active' => 1,
                'list_order' => 7,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1314,
                'name' => 'agnes b',
                'is_active' => 1,
                'list_order' => 16,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1315,
                'name' => 'Abercrombie・・itch',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1316,
                'name' => 'ICEBERG',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1317,
                'name' => 'ISSEY MIYAKE',
                'is_active' => 1,
                'list_order' => 83,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1318,
                'name' => 'Yves Saint Laurent',
                'is_active' => 1,
                'list_order' => 156,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1319,
                'name' => 'OAKLEY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1320,
                'name' => 'Cartier',
                'is_active' => 1,
                'list_order' => 9,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1321,
                'name' => 'CASTELBAJAC',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1322,
                'name' => 'chloe',
                'is_active' => 1,
                'list_order' => 41,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1323,
                'name' => 'CHROME HEARTS',
                'is_active' => 1,
                'list_order' => 45,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1324,
                'name' => 'Salvatore Ferragamo',
                'is_active' => 1,
                'list_order' => 129,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1325,
                'name' => 'Samantha Thavasa',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1326,
                'name' => 'JEAN PAUL GAULTIER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1327,
                'name' => 'JILLSTUART',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1328,
                'name' => 'CELINE',
                'is_active' => 1,
                'list_order' => 38,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1329,
                'name' => 'SONIARYKIEL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1330,
                'name' => 'DKNY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1331,
                'name' => 'dunhill',
                'is_active' => 1,
                'list_order' => 57,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1332,
                'name' => 'TAKEOKIKUCHI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1333,
                'name' => 'TOMMY HILFIGER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1334,
                'name' => 'DIESEL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1335,
                'name' => 'DSQUARED2',
                'is_active' => 1,
                'list_order' => 56,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1336,
                'name' => 'D・・',
                'is_active' => 1,
                'list_order' => 51,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1337,
                'name' => 'BURBERRY',
                'is_active' => 1,
                'list_order' => 32,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1338,
                'name' => 'Ralph LAUREN',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1339,
                'name' => 'Christian Louboutin',
                'is_active' => 1,
                'list_order' => 44,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1340,
                'name' => 'LeSportsac',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1341,
                'name' => 'RayBan',
                'is_active' => 1,
                'list_order' => 120,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1342,
                'name' => 'ROPE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1343,
                'name' => 'LOEWE',
                'is_active' => 1,
                'list_order' => 95,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1344,
                'name' => 'LONG CHAMP',
                'is_active' => 1,
                'list_order' => 96,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1345,
                'name' => 'UNITED ARROWS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1346,
                'name' => 'MARC JACOBS',
                'is_active' => 1,
                'list_order' => 100,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1347,
                'name' => 'MICHEL KLEIN',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1348,
                'name' => 'miu miu',
                'is_active' => 1,
                'list_order' => 107,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1349,
                'name' => 'MOSCHINO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1350,
                'name' => 'MONCLER',
                'is_active' => 1,
                'list_order' => 108,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1351,
                'name' => 'Banana Republic',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1352,
                'name' => 'BALENCIAGA',
                'is_active' => 1,
                'list_order' => 22,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1353,
                'name' => 'BALLY',
                'is_active' => 1,
                'list_order' => 23,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1354,
                'name' => 'PRADA',
                'is_active' => 1,
                'list_order' => 5,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1355,
                'name' => 'FENDI',
                'is_active' => 1,
                'list_order' => 65,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1356,
                'name' => 'VERSACE',
                'is_active' => 1,
                'list_order' => 153,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1357,
                'name' => 'Paul Smith',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1358,
                'name' => 'NARACAMICIE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1359,
                'name' => '23蛹ｺ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1360,
                'name' => 'SAZABY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1361,
                'name' => 'CHARLES JOURDAN',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1362,
                'name' => 'GIVENCHY',
                'is_active' => 1,
                'list_order' => 76,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1363,
                'name' => 'JIMMY CHOO',
                'is_active' => 1,
                'list_order' => 90,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1364,
                'name' => 'JILSANDER',
                'is_active' => 1,
                'list_order' => 89,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1365,
                'name' => 'FURLA',
                'is_active' => 1,
                'list_order' => 71,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1366,
                'name' => 'Kate Spade',
                'is_active' => 1,
                'list_order' => 93,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1367,
                'name' => 'FRANCESCO BIASIA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1368,
                'name' => 'STAR JEWELRY',
                'is_active' => 1,
                'list_order' => 135,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1369,
                'name' => 'Vivienne Westwood',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1370,
                'name' => 'Tiffany・・o.',
                'is_active' => 1,
                'list_order' => 11,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1371,
                'name' => 'Ritmo latino',
                'is_active' => 1,
                'list_order' => 124,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1372,
                'name' => 'Folli Follie',
                'is_active' => 1,
                'list_order' => 66,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1373,
                'name' => 'Chopard',
                'is_active' => 1,
                'list_order' => 42,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1374,
                'name' => 'Max・・o.',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1375,
                'name' => 'MaxMara',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1376,
                'name' => 'KRIZIA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1377,
                'name' => 'TUMI',
                'is_active' => 1,
                'list_order' => 145,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1378,
                'name' => 'TAG Heuer',
                'is_active' => 1,
                'list_order' => 8,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1379,
                'name' => 'SEIKO',
                'is_active' => 1,
                'list_order' => 131,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1380,
                'name' => 'MONTBLANC',
                'is_active' => 1,
                'list_order' => 109,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1381,
                'name' => 'AIGNER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1382,
                'name' => 'EMILIO PUCCI',
                'is_active' => 1,
                'list_order' => 60,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1383,
                'name' => 'Baccarat',
                'is_active' => 1,
                'list_order' => 21,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1384,
                'name' => 'EMBA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1385,
                'name' => 'LONGINES',
                'is_active' => 1,
                'list_order' => 97,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1386,
                'name' => 'THE NORTH FACE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1387,
                'name' => 'Felisi',
                'is_active' => 1,
                'list_order' => 64,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1388,
                'name' => 'TOD窶儡',
                'is_active' => 1,
                'list_order' => 141,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1389,
                'name' => 'WEDGWOOD',
                'is_active' => 1,
                'list_order' => 155,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1390,
                'name' => 'SWAROVSKI',
                'is_active' => 1,
                'list_order' => 137,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1391,
                'name' => 'BOTTEGA VENETA',
                'is_active' => 1,
                'list_order' => 27,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1392,
                'name' => 'BAUME・・ERCIER',
                'is_active' => 1,
                'list_order' => 24,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1393,
                'name' => 'TO BE CHIC',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1394,
                'name' => 'MINNETONKA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1395,
                'name' => 'VALENTINO',
                'is_active' => 1,
                'list_order' => 150,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1396,
                'name' => 'PRIMA CLASSE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1397,
                'name' => 'Aquascutum',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1398,
                'name' => 'TENDENCE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1399,
                'name' => 'REDWING',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1400,
                'name' => 'Carrera y Carrera',
                'is_active' => 1,
                'list_order' => 35,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1401,
                'name' => 'HYSTERIC GLAMOUR',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1402,
                'name' => 'MIKIHOUSE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1403,
                'name' => 'Justin Davis',
                'is_active' => 1,
                'list_order' => 91,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1404,
                'name' => 'BOSE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1405,
                'name' => 'Canon',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1406,
                'name' => 'CASIO',
                'is_active' => 1,
                'list_order' => 36,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1407,
                'name' => 'dyson',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1408,
                'name' => 'HITACHI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1409,
                'name' => 'makita',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1410,
                'name' => 'SIGMA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1411,
                'name' => 'PENTAX',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1412,
                'name' => 'TOSHIBA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1413,
                'name' => '蜊大ｼ･蜻ｼ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1414,
                'name' => 'Samantha Vega',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1415,
                'name' => 'HUNTING WORLD',
                'is_active' => 1,
                'list_order' => 82,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1416,
                'name' => 'ETRO',
                'is_active' => 1,
                'list_order' => 62,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1417,
                'name' => 'IBIZA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1418,
                'name' => 'Cole Haan',
                'is_active' => 1,
                'list_order' => 48,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1419,
                'name' => 'Orobianco',
                'is_active' => 1,
                'list_order' => 111,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1420,
                'name' => 'GHERARDINI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1421,
                'name' => 'CLATHAS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1422,
                'name' => 'S.T. Dupont',
                'is_active' => 1,
                'list_order' => 127,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1423,
                'name' => 'Anya Hindmarch',
                'is_active' => 1,
                'list_order' => 18,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1424,
                'name' => 'Mulberry',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1425,
                'name' => 'INDIVI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1426,
                'name' => 'LANVIN',
                'is_active' => 1,
                'list_order' => 94,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1427,
                'name' => 'SONY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1428,
                'name' => 'PUMA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1429,
                'name' => 'Kitamura',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1430,
                'name' => 'MARIO VALENTINO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1431,
                'name' => 'Noritake',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1432,
                'name' => '莨雁､ｪ蛻ｩ螻・,1,0,2017-10-22 03:40=>16\'',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1433,
                'name' => 'FRED',
                'is_active' => 1,
                'list_order' => 69,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1434,
                'name' => 'Cath Kidston',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1435,
                'name' => 'DIANA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1436,
                'name' => 'ZERO HALLIBURTON',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1437,
                'name' => 'EMPORIO ARMANI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1438,
                'name' => 'GIORGIO ARMANI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1439,
                'name' => 'apple',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1440,
                'name' => 'Nintendo',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1441,
                'name' => 'A BATHING APE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1442,
                'name' => 'LeCreuset',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1443,
                'name' => 'Ermenegildo Zegna',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1444,
                'name' => 'Alexandre de Paris',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1445,
                'name' => 'OLYMPUS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1446,
                'name' => 'MARC BY MARC JACOBS',
                'is_active' => 1,
                'list_order' => 99,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1447,
                'name' => 'SEE BY CHLOE',
                'is_active' => 1,
                'list_order' => 130,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1448,
                'name' => 'Panasonic',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1449,
                'name' => 'lucien pellat-finet',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1450,
                'name' => 'TUDOR',
                'is_active' => 1,
                'list_order' => 144,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1451,
                'name' => 'FOXEY',
                'is_active' => 1,
                'list_order' => 67,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1452,
                'name' => 'MCM',
                'is_active' => 1,
                'list_order' => 103,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1453,
                'name' => 'TAMRON',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1454,
                'name' => 'PARKER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1455,
                'name' => 'RODANIA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1456,
                'name' => 'CHARRIOL',
                'is_active' => 1,
                'list_order' => 39,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1457,
                'name' => 'Harrods',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1458,
                'name' => 'LEONARD',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1459,
                'name' => 'NIKE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1460,
                'name' => 'ROYAL COPENHAGEN',
                'is_active' => 1,
                'list_order' => 126,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1461,
                'name' => 'ZIPPO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1462,
                'name' => 'SAMSONITE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1463,
                'name' => 'NINA RICCI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1464,
                'name' => 'FUJITSU',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1465,
                'name' => 'HIROFU',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1466,
                'name' => 'National',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1467,
                'name' => 'Meissen',
                'is_active' => 1,
                'list_order' => 104,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1468,
                'name' => 'Richard Ginori',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1469,
                'name' => 'CITIZEN',
                'is_active' => 1,
                'list_order' => 46,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1470,
                'name' => 'POMELLATO',
                'is_active' => 1,
                'list_order' => 117,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1471,
                'name' => 'FRANCK MULLER',
                'is_active' => 1,
                'list_order' => 68,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1472,
                'name' => 'JEAN PATOU',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1473,
                'name' => 'RIMOWA',
                'is_active' => 1,
                'list_order' => 123,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1474,
                'name' => 'PAUL・・OE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1475,
                'name' => 'RAYMOND WEIL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1476,
                'name' => 'CORUM',
                'is_active' => 1,
                'list_order' => 50,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1477,
                'name' => 'Nikon',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1478,
                'name' => 'Van Cleef・・rpels',
                'is_active' => 1,
                'list_order' => 13,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1479,
                'name' => 'Kitson',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1480,
                'name' => 'RADO',
                'is_active' => 1,
                'list_order' => 119,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1481,
                'name' => 'MIKIMOTO',
                'is_active' => 1,
                'list_order' => 106,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1482,
                'name' => 'ORIENT',
                'is_active' => 1,
                'list_order' => 110,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1483,
                'name' => 'BARNEYS NEW YORK',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1484,
                'name' => 'Selmer',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1485,
                'name' => 'theory',
                'is_active' => 1,
                'list_order' => 139,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1486,
                'name' => 'SHARP',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1487,
                'name' => 'Christofle',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1488,
                'name' => 'SPORTMAX',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1489,
                'name' => 'NEC',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1490,
                'name' => 'Valentino Garavani',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1491,
                'name' => 'Aynsley',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1492,
                'name' => 'MITSUBISHI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1493,
                'name' => 'NARUMI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1494,
                'name' => 'HEREND',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1495,
                'name' => 'ROYAL DOULTON',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1496,
                'name' => 'TRUSSARDI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1497,
                'name' => 'Disney',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1498,
                'name' => 'Sergio Rossi',
                'is_active' => 1,
                'list_order' => 132,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1499,
                'name' => 'MINTON',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1500,
                'name' => 'DAIWA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1501,
                'name' => 'REGAL',
                'is_active' => 1,
                'list_order' => 121,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1502,
                'name' => 'PIAGET',
                'is_active' => 1,
                'list_order' => 114,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1503,
                'name' => 'LLADRO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1504,
                'name' => 'courreges',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1505,
                'name' => 'Timberland',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1506,
                'name' => 'Ponte Vecchio',
                'is_active' => 1,
                'list_order' => 118,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1507,
                'name' => 'BREE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1508,
                'name' => 'Georg Jensen',
                'is_active' => 1,
                'list_order' => 73,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1509,
                'name' => 'MINOLTA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1510,
                'name' => 'MACKINTOSH',
                'is_active' => 1,
                'list_order' => 98,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1511,
                'name' => 'M-premier',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1512,
                'name' => 'GINZA Kanematsu',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1513,
                'name' => 'EPOCA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1514,
                'name' => 'POLICE',
                'is_active' => 1,
                'list_order' => 116,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1515,
                'name' => 'FEILER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1516,
                'name' => 'KUMIKYOKU',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1517,
                'name' => 'russet',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1518,
                'name' => '4邃・,1,158,2017-10-22 03:40=>16\'',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1519,
                'name' => 'Pioneer',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1520,
                'name' => 'FUJIFILM',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1521,
                'name' => 'KIPLING',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1522,
                'name' => 'Bruno Magli',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1523,
                'name' => 'JUICY COUTURE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1524,
                'name' => 'Tom Ford',
                'is_active' => 1,
                'list_order' => 142,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1525,
                'name' => 'TASAKI',
                'is_active' => 1,
                'list_order' => 138,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1526,
                'name' => 'ED HARDY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1527,
                'name' => 'ZOJIRUSHI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1528,
                'name' => 'Vendome Aoyama',
                'is_active' => 1,
                'list_order' => 151,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1529,
                'name' => 'mila schon',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1530,
                'name' => 'SHIMANO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1531,
                'name' => 'IWC',
                'is_active' => 1,
                'list_order' => 84,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1532,
                'name' => 'LANCEL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1533,
                'name' => 'PORTER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1534,
                'name' => 'John Galliano',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1535,
                'name' => 'SANYO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1536,
                'name' => 'BORBONESE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1537,
                'name' => 'Tricker窶冱',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1538,
                'name' => 'POLO Ralph Lauren',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1539,
                'name' => 'Tory Burch',
                'is_active' => 1,
                'list_order' => 143,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1540,
                'name' => 'HAMILTON',
                'is_active' => 1,
                'list_order' => 80,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1541,
                'name' => 'DIANE von FURSTENBERG',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1542,
                'name' => 'ARMANI COLLEZIONI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1543,
                'name' => 'ARMANI EXCHANGE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1544,
                'name' => 'Pinky・・ianne',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1545,
                'name' => 'UNOAERRE',
                'is_active' => 1,
                'list_order' => 148,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1546,
                'name' => 'Samantha Tiara',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1547,
                'name' => 'ANNA SUI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1548,
                'name' => 'GIANFRANCO FERRE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1549,
                'name' => 'UGG',
                'is_active' => 1,
                'list_order' => 146,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1550,
                'name' => 'ROYAL ALBERT',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1551,
                'name' => 'COMME des GARCONS',
                'is_active' => 1,
                'list_order' => 49,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1552,
                'name' => 'Dior',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1553,
                'name' => 'ESCADA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1554,
                'name' => 'PLEATS PLEASE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1555,
                'name' => 'PATRICK COX',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1556,
                'name' => 'CARAN dACHE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1557,
                'name' => 'WATERMAN',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1558,
                'name' => 'Roberta di Camerino',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1559,
                'name' => 'SHEAFFER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1560,
                'name' => 'GOLD PFEIL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1561,
                'name' => 'Think Bee・・,1,0,2017-10-22 03:40=>16\'',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1562,
                'name' => 'Burberrys',
                'is_active' => 1,
                'list_order' => 33,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1563,
                'name' => 'DUVETICA',
                'is_active' => 1,
                'list_order' => 58,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1564,
                'name' => 'MARNI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1565,
                'name' => 'MISSONI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1566,
                'name' => 'RON HERMAN',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1567,
                'name' => 'BOUCHERON',
                'is_active' => 1,
                'list_order' => 28,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1568,
                'name' => 'CHAUMET',
                'is_active' => 1,
                'list_order' => 40,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1569,
                'name' => 'A. LANGE & SOHNE',
                'is_active' => 1,
                'list_order' => 14,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1570,
                'name' => 'AUDEMARS PIGUET',
                'is_active' => 1,
                'list_order' => 20,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1571,
                'name' => 'Bell & Ross',
                'is_active' => 1,
                'list_order' => 25,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1572,
                'name' => 'Blancpain',
                'is_active' => 1,
                'list_order' => 26,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1573,
                'name' => 'BREGUET',
                'is_active' => 1,
                'list_order' => 29,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1574,
                'name' => 'BREITLING',
                'is_active' => 1,
                'list_order' => 30,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1575,
                'name' => 'Carlo Parlati',
                'is_active' => 1,
                'list_order' => 34,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1576,
                'name' => 'CAZZANIGA',
                'is_active' => 1,
                'list_order' => 37,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1577,
                'name' => 'Damiani',
                'is_active' => 1,
                'list_order' => 52,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1578,
                'name' => 'DE BEERS',
                'is_active' => 1,
                'list_order' => 53,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1579,
                'name' => 'EDOX',
                'is_active' => 1,
                'list_order' => 59,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1580,
                'name' => 'FEDERICO BUCCELLATI',
                'is_active' => 1,
                'list_order' => 63,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1581,
                'name' => 'FREDERIQUE CONSTANT',
                'is_active' => 1,
                'list_order' => 70,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1582,
                'name' => 'gimel',
                'is_active' => 1,
                'list_order' => 74,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1583,
                'name' => 'GIRARD PERREGAUX',
                'is_active' => 1,
                'list_order' => 75,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1584,
                'name' => 'GOYARD',
                'is_active' => 1,
                'list_order' => 77,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1585,
                'name' => 'GRAFF',
                'is_active' => 1,
                'list_order' => 78,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1586,
                'name' => 'GRAHAM',
                'is_active' => 1,
                'list_order' => 79,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1587,
                'name' => 'HARRY WINSTON',
                'is_active' => 1,
                'list_order' => 12,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1588,
                'name' => 'HUBLOT',
                'is_active' => 1,
                'list_order' => 81,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1589,
                'name' => 'JAEGER LECOULTRE',
                'is_active' => 1,
                'list_order' => 86,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1590,
                'name' => 'JEWEL STUDIO',
                'is_active' => 1,
                'list_order' => 88,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1591,
                'name' => 'Jeunet',
                'is_active' => 1,
                'list_order' => 87,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1592,
                'name' => 'Kashikey',
                'is_active' => 1,
                'list_order' => 92,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1593,
                'name' => 'MAUBOUSSIN',
                'is_active' => 1,
                'list_order' => 101,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1594,
                'name' => 'MELLERIO dits MELLER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1595,
                'name' => 'ARMANI JEANS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1596,
                'name' => 'NIXON',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1597,
                'name' => 'IWC',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1598,
                'name' => 'SINN',
                'is_active' => 1,
                'list_order' => 133,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1599,
                'name' => 'MAURICE LACROIX',
                'is_active' => 1,
                'list_order' => 102,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1600,
                'name' => 'PANERAI',
                'is_active' => 1,
                'list_order' => 112,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1601,
                'name' => 'PATEK PHILIPPE',
                'is_active' => 1,
                'list_order' => 113,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1602,
                'name' => 'POLA',
                'is_active' => 1,
                'list_order' => 115,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1603,
                'name' => 'RICHARD MILLE',
                'is_active' => 1,
                'list_order' => 122,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1604,
                'name' => 'ROGER DUBUIS',
                'is_active' => 1,
                'list_order' => 125,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1605,
                'name' => 'SOUTHERN CROSS',
                'is_active' => 1,
                'list_order' => 134,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1606,
                'name' => 'ULYSSE NARDIN',
                'is_active' => 1,
                'list_order' => 147,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1607,
                'name' => 'VACHERON CONSTANTIN',
                'is_active' => 1,
                'list_order' => 149,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1608,
                'name' => 'ZENITH',
                'is_active' => 1,
                'list_order' => 157,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1609,
                'name' => '遏ｳ蟾・證｢蟄・,1,161,2017-10-22 03:40=>16\'',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1610,
                'name' => '譴ｶ 蜈牙､ｫ',
                'is_active' => 1,
                'list_order' => 159,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1611,
                'name' => '逕ｰ譚・菫贋ｸ',
                'is_active' => 1,
                'list_order' => 162,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1612,
                'name' => 'SAINT LAURENT',
                'is_active' => 1,
                'list_order' => 128,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1613,
                'name' => '縺昴・莉・,1,163,2017-10-22 03:40=>16\'',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1614,
                'name' => '蟯ｩ蛟・蠎ｷ莠・,1,0,0,2021-07-08 02=>07=>23\'',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1615,
                'name' => '隨蜴・逵溷ｯｿ鄒・,1,0,0,2021-07-08 02=>07=>23\'',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1616,
                'name' => 'FOREVERMARK',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1617,
                'name' => 'ilias LALAoUNIS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1618,
                'name' => 'GIANMARIA BUCCELLATI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1619,
                'name' => 'agete',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1620,
                'name' => 'AHKAH',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1621,
                'name' => 'SYMPATHY OF SOUL',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1622,
                'name' => '豎逕ｰ 蝠灘ｭ・',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1623,
                'name' => '闖・ｲｼ 遏･陦・',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1624,
                'name' => 'Wellendorff',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1625,
                'name' => 'NIWAKA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1626,
                'name' => 'ROYAL ASSCHER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1627,
                'name' => 'TSUTSUMI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1628,
                'name' => '繧ｱ繧､繧ｦ繝・,0,0,0,2021-07-08 02=>07=>23\'',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1629,
                'name' => '荳芽ｶ・',
                'is_active' => 0,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1630,
                'name' => 'GINZA TANAKA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1631,
                'name' => 'WAKO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1632,
                'name' => 'ANTONINI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1633,
                'name' => '讙應ｽ・雉｢豐ｻ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1634,
                'name' => 'EYEFUNNY',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1635,
                'name' => '譛ｨ蜀・雉｢豐ｻ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1636,
                'name' => 'Loree Rodkin',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1637,
                'name' => 'CHIMENTO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1638,
                'name' => 'KARATI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1639,
                'name' => 'Patrizia Parlati',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1640,
                'name' => '遖丞次 菴先匱',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1641,
                'name' => 'MARIO BUCCELLATI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1642,
                'name' => 'HermanMiller',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1643,
                'name' => 'cassina',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1644,
                'name' => 'arflex',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1645,
                'name' => 'ACTUS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1646,
                'name' => 'ligne roset',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1647,
                'name' => '繧ｫ繝ｪ繝｢繧ｯ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1648,
                'name' => '螟ｩ遶･譛ｨ蟾･',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1649,
                'name' => 'Fritz Hansen',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1650,
                'name' => 'Dior',
                'is_active' => 1,
                'list_order' => 54,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1651,
                'name' => 'Queen',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1652,
                'name' => 'WALTHAM',
                'is_active' => 1,
                'list_order' => 154,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1653,
                'name' => 'AGATHA',
                'is_active' => 1,
                'list_order' => 15,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1654,
                'name' => 'Asprey',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1655,
                'name' => 'BLUERIVER',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1656,
                'name' => 'BUCCELLATI',
                'is_active' => 1,
                'list_order' => 31,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1657,
                'name' => 'Christian Bernard',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1658,
                'name' => 'Chronoswiss',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1659,
                'name' => 'CRESCENT VERT',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1660,
                'name' => 'GaGa MILANO',
                'is_active' => 1,
                'list_order' => 72,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1661,
                'name' => 'Jacob&co',
                'is_active' => 1,
                'list_order' => 85,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1662,
                'name' => 'K.uno',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1663,
                'name' => 'MASRIERA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1664,
                'name' => 'Michael Kors',
                'is_active' => 1,
                'list_order' => 105,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1665,
                'name' => 'MITSUWA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1666,
                'name' => 'NOMBRE',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1667,
                'name' => 'PICCHIOTTI',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1668,
                'name' => 'PIERRE KUNZ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1669,
                'name' => 'RITMO MVNDO',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1670,
                'name' => 'ROCCA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1671,
                'name' => 'ROMAIN JEROME',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1672,
                'name' => 'RUGIADA',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1673,
                'name' => 'Supreme',
                'is_active' => 1,
                'list_order' => 136,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1674,
                'name' => 'TISSOT',
                'is_active' => 1,
                'list_order' => 140,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1675,
                'name' => 'TOMOYAS',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1676,
                'name' => 'Verite',
                'is_active' => 1,
                'list_order' => 152,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1677,
                'name' => 'Yanes',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1678,
                'name' => 'Zoccai',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1679,
                'name' => '譴ｶ豁ｦ蜿ｲ',
                'is_active' => 1,
                'list_order' => 160,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1680,
                'name' => '蟆丞ｯｺ譎ｺ蟄・',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1681,
                'name' => '豌ｴ驥手稔蟄・',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1682,
                'name' => '譏溘・遐・',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1683,
                'name' => '譚第收蜿ｸ',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1684,
                'name' => '蟷ｳ蜥悟・',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('10/22/2017 3:40')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1685,
                'name' => 'ete',
                'is_active' => 1,
                'list_order' => 61,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ],
            [
                'external_id' => 1686,
                'name' => 'deLaCour',
                'is_active' => 1,
                'list_order' => 0,
                'created' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07')),
                'modified' => date('Y-m-d H:i:s', strtotime('7/8/2021 2:07'))
            ]
        ]);
    }
}
