# DASHBOARD CONSOLE
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It was designed with material ui.

### Manual Running steps without docker
1. Cloning Project
```bash
# clone project
git clone https://gitlab.com/khaingmintunn/dashboard.git

# redirect to backend folder
cd dashboard
cd dashboard-console
```

2. Modify env config
- create `.env` file and copy code from `.env.example` in root redictory.
- modify `REACT_APP_ENDPOINT`, `REACT_APP_NAME` and `REACT_APP_SALT_KEY`.

3. Open command prompt or terminal
```bash
# install dependicies and packages with npm
$ npm install

# install dependicies and packages with yarn
$ yarn install

# run the app in development mode with npm
$ npm start

# run the app in development mode with yarn
$ yarn start
```
**Open [http://localhost:3000](http://localhost:3000) to view it in the browser.**

The page will automatically reload if you make changes to the code.<br>
You will see the build errors and lint warnings in the console.


### 

Runs the test watcher in an interactive mode.<br>
By default, runs tests related to files changed since the last commit.
```bash
# test with npm
$ npm test

# test with yarn
$ yarn test
```

[Read more about testing.](https://facebook.github.io/create-react-app/docs/running-tests)



Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
```bash
# build app with npm
$ npm build

# build app with yarn
$ yarn build
```
The build is minified and the filenames include the hashes.<br>

Your app is ready to be deployed.

## User Guide

You can find detailed instructions on using Create React App and many tips in [its documentation](https://facebook.github.io/create-react-app/).