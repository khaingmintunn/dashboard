import {
  Box,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Table,
} from '@material-ui/core';
import { ReactNode } from 'react';
import StyledTableCell from '../StyledTableCell';

interface TableProps {
  // header row node list
  header?: ReactNode
  // body row node list
  body?: ReactNode
}

const MyTable = ({ header, body }: TableProps) => (
  <>
    <Box>
      <Table>
        <TableHead>
          {header}
        </TableHead>
        <TableBody sx={{ backgroundColor: '#fff' }}>
          {body}
        </TableBody>
      </Table>
    </Box>
  </>
);

export { MyTable as Table, TableRow, StyledTableCell as TableHeaderCell, TableCell };
