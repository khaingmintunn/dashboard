import {
  Box,
  // Button,
  TablePagination,
  Typography,
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import type { ChangeEvent, MouseEvent } from 'react';
import StyledTableSortLabel from './StyledTableSortLabel';
import {
  Table,
  TableRow,
  TableHeaderCell,
  TableCell
} from 'src/components/atoms/Table';
import _ from 'lodash';
import { useNavigate } from 'react-router-dom';

interface TableProps {
  columns: any[];
  tablebody: any[];
  isSorted: boolean;
  sortedDirection?: Array<{ name: string, dir: 'asc' | 'desc' }>;
  // Whether you want to activate pagination feature
  isPagination: boolean;
}

const TableComponent = (props: TableProps) => {
  const { columns, tablebody, isSorted, isPagination, sortedDirection } = props;
  // Pagination: Page
  const [page, setPage] = useState<number>(0);
  // Pagination: How many you want to show
  const [limit, setLimit] = useState<number>(10);
  // Direction
  const [direct, setDirect] = useState([]);
  const navigate = useNavigate();
  // Default sort value
  const [sortCoVal, setSortCoVal] = useState<string>('');
  useEffect(() => {
    const setCol = () => {
      setSortCoVal(sortedDirection[0].name);
      setDirect(sortedDirection);
    };
    if (sortedDirection) setCol();
  }, [sortedDirection]);

  let paginatedOrgList: Array<[]> = [];

  const handleSort = (columnName: string): void => {
    setSortCoVal(columnName);
    const index = _.findIndex(sortedDirection, ['name', columnName]);
    const cloneDir = _.cloneDeep(sortedDirection);
    setDirect((preDir) => {
      if (preDir[index].dir === 'asc') cloneDir[index].dir = 'desc';
      else cloneDir[index].dir = 'asc';
      return cloneDir;
    });
  };
  const handlePageChange = (event: MouseEvent<HTMLButtonElement> | null, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value, 10));
  };

  const getSortedOrgList = (
    lst: any[],
    orders: Array<{ name: string, dir: 'asc' | 'desc' }>,
    sortVal: string
  ): any[] => lst
    .sort((a, b) => {
      const index = _.findIndex(orders, ['name', sortVal]);
      if (index === -1) return 0;
      if (orders[index].dir === 'asc') {
        return a[sortVal] < b[sortVal] ? -1 : 1;
      }

      return a[sortVal] > b[sortVal] ? -1 : 1;
    });

  const applyPagination = (
    customers: any[],
    pages: number,
    limits: number
  ): any[] => customers
    .slice(pages * limits, pages * limits + limits);

  if (isPagination) {
    const sortedOrgList = isSorted ? getSortedOrgList(tablebody, direct, sortCoVal) : tablebody;
    paginatedOrgList = applyPagination(sortedOrgList, page, limit);
  } else {
    paginatedOrgList = isSorted ? getSortedOrgList(tablebody, direct, sortCoVal) : tablebody;
  }
  return (
    <>
      <Box>
        <Table
          header={(
            <TableRow>
              <TableHeaderCell />
              {columns.map((col) => {
                let cell: any;
                const index = _.findIndex(direct, ['name', col.colVal]);
                if (!isSorted || index === -1) {
                  cell = (
                    <TableHeaderCell
                      key={col.colName}
                      width={col?.width}
                    >
                      {col.colName}
                    </TableHeaderCell>
                  );
                } else {
                  cell = (
                    <TableHeaderCell
                      key={col.colName}
                      sortDirection={direct[index].dir}
                      width={col?.width}
                    >
                      <StyledTableSortLabel
                        active
                        onClick={() => {
                          handleSort(col.colVal);
                        }}
                        direction={direct[index].dir}
                      >
                        {col.colName}
                      </StyledTableSortLabel>
                    </TableHeaderCell>
                  );
                }
                return (
                  cell
                );
              })}
            </TableRow>
          )}
          body={(
            <>
              {paginatedOrgList.map((lst: any, i) => (
                <TableRow key={lst?.id}>
                  <TableCell>{(page * limit) + (i + 1)}</TableCell>
                  {columns.map((keys) => (
                    <TableCell
                      key={keys.colVal}
                      width={keys?.width}
                    >
                      {keys?.isEdit ? (
                        <Typography
                          sx={{
                            fontFamily: 'serif',
                            color: '#0000ff',
                            fontWeight: 'bold',
                            cursor: 'pointer'
                          }}
                          onClick={() => navigate(`/master/${lst.id}`)}
                        >
                          Detail
                        </Typography>
                      )
                        : lst[keys.colVal]}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </>
          )}
        />
      </Box>
      {
        isPagination
        && (
          <TablePagination
            component="div"
            labelRowsPerPage="Rows:"
            count={tablebody.length}
            onPageChange={handlePageChange}
            onRowsPerPageChange={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[10, 20, 30]}
          />
        )
      }
    </>
  );
};

export default TableComponent;
