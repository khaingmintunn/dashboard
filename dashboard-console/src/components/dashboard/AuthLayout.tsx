import { FC, ReactNode, useState } from 'react';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { experimentalStyled } from '@material-ui/core/styles';
import DashboardNavbar from './DashboardNavbar';
import { getItem } from 'src/utils/encrypt';

interface DashboardLayoutProps {
  children?: ReactNode;
}

export const DashboardLayoutRoot = experimentalStyled('div')(
  ({ theme }) => (
    {
      backgroundColor: theme.palette.background.default,
      display: 'flex',
      height: '100%',
      overflow: 'hidden',
      width: '100%'
    }
  )
);

export const DashboardLayoutWrapper = experimentalStyled('div')(
  ({ theme }) => (
    {
      display: 'flex',
      flex: '1 1 auto',
      overflow: 'hidden',
      paddingTop: '64px',
      [theme.breakpoints.up('lg')]: {
        paddingLeft: '280px'
      }
    }
  )
);

export const DashboardLayoutContainer = experimentalStyled('div')({
  display: 'flex',
  flex: '1 1 auto',
  overflow: 'hidden'
});

export const DashboardLayoutContent = experimentalStyled('div')({
  flex: '1 1 auto',
  height: '100%',
  overflow: 'auto',
  position: 'relative',
  WebkitOverflowScrolling: 'touch'
});

const AuthLayout: FC<DashboardLayoutProps> = () => {
  const location = useLocation();
  const [requestedLocation, setRequestedLocation] = useState(null);
  const auth: any = getItem(process.env.REACT_APP_NAME);
  console.log(auth);

  if (!auth?.user) {
    return <Navigate to="/login" />;
  }

  if (requestedLocation && location.pathname !== requestedLocation) {
    setRequestedLocation(null);
    return <Navigate to={requestedLocation} />;
  }

  return (
    <DashboardLayoutRoot>
      <DashboardNavbar />

      <Outlet />
    </DashboardLayoutRoot>
  );
};

export default AuthLayout;
