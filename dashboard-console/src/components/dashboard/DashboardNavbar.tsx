import { FC } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { AppBar, Box, Button, Card, Toolbar, Typography } from '@material-ui/core';
import { experimentalStyled } from '@material-ui/core/styles';
import type { AppBarProps } from '@material-ui/core';
import { logout } from 'src/services/auth';
import { getItem } from 'src/utils/encrypt';

interface DashboardNavbarProps extends AppBarProps {
  onSidebarMobileOpen?: () => void;
}

const DashboardNavbarRoot = experimentalStyled(AppBar)(
  ({ theme }) => (
    {
      ...(
        theme.palette.mode === 'light' && {
          backgroundColor: theme.palette.common.white,
          boxShadow: 'none',
          color: theme.palette.primary.contrastText
        }
      ),
      ...(
        theme.palette.mode === 'dark' && {
          backgroundColor: theme.palette.background.paper,
          borderBottom: `1px solid ${theme.palette.divider}`,
          boxShadow: 'none'
        }
      ),
      zIndex: theme.zIndex.drawer + 100
    }
  )
);

const DashboardNavbar: FC<DashboardNavbarProps> = (props) => {
  const { onSidebarMobileOpen, ...other } = props;
  const navigate = useNavigate();
  const data: any = getItem(process.env.REACT_APP_NAME);

  const onClickLogout = async () => {
    await logout();
    localStorage.clear();
    navigate('/login', { replace: true });
  };

  return (
    <DashboardNavbarRoot
      {...other}
      sx={{
        borderBottom: '1px solid rgb(0 0 0 / 12%)'
      }}
      style={{
        boxShadow: '3px 3px 6px #e5e2de'
      }}
    >
      <Toolbar sx={{ minHeight: 64 }}>
        <RouterLink to="/master">
          <Typography
            sx={{
              color: '#0000ff',
              fontSize: 30,
              textAlign: 'center',
              fontWeight: 'bold',
              fontFamily: 'sans-serif',
              fontVariant: 'small-caps',
              textShadow: '2px 2px #99aa99',
              textDecoration: 'none'
            }}
          >
            Dashboard
          </Typography>
        </RouterLink>
        <Box
          sx={{
            flexGrow: 1,
            ml: 2
          }}
        />
        <Card
          sx={{
            p: 1
          }}
        >
          <Typography>
            <strong>
              {data?.user?.name}
            </strong>
          </Typography>
        </Card>
        <Button
          onClick={onClickLogout}
          sx={{
            ml: 2
          }}
        >
          Logout
        </Button>
      </Toolbar>
    </DashboardNavbarRoot>
  );
};

DashboardNavbar.propTypes = {
  onSidebarMobileOpen: PropTypes.func
};

export default DashboardNavbar;
