export interface User {
  id: string;
  email: string;
  name: string;
  password: string;
  email_verified_at: string;
  created_at: string;
  updated_at: string;
}
