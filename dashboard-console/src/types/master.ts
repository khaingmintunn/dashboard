export interface Master {
  id: number;
  external_id: number;
  name: string;
  is_active: number;
  list_order: number;
  created: string;
  modified: string;
}
