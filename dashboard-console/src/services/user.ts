import axios from 'axios';
import { User } from 'src/types/user';
import { getItem, setItem } from 'src/utils/encrypt';

export const getUser = async () => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const users: User[] = await new Promise((resolve, reject) => {
    axios.get(`${process.env.REACT_APP_ENDPOINT}/users`,
      { headers: { Authorization: authedPayload?.token } })
      .then((response) => {
        resolve(response.data.users);
      })
      .catch((error) => {
        reject(error.response.message);
      });
  });

  return users;
};

export const getProfile = async () => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const user: User = await new Promise((resolve, reject) => {
    axios.get(`${process.env.REACT_APP_ENDPOINT}/profile`,
      { headers: { Authorization: authedPayload?.token } })
      .then((response) => {
        resolve(response.data.user);
      })
      .catch((error) => {
        reject(error.response.message);
      });
  });

  return user;
};

export const updateProfile = async (data: any) => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const user: User = await new Promise((resolve) => {
    axios.put(`${process.env.REACT_APP_ENDPOINT}/profile`,
      data,
      {
        headers: {
          Accept: 'application/json',
          Authorization: authedPayload?.token
        }
      })
      .then((response) => {
        setItem(process.env.REACT_APP_NAME, { token: authedPayload?.token, user: response.data.user });
        resolve(response.data.user);
      })
      .catch((error) => {
        resolve(error.response.data);
      });
  });

  return user;
};
