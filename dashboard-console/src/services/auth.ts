import axios from 'axios';
import { getItem, setItem } from 'src/utils/encrypt';

export const login = async (params: any) => {
  const data: any = await new Promise((resolve) => {
    axios.post(`${process.env.REACT_APP_ENDPOINT}/login`,
      params,
      { headers: { Accept: 'application/json' } })
      .then((response) => {
        setItem(process.env.REACT_APP_NAME, response.data);
        resolve(response.data);
      })
      .catch((error) => {
        resolve(error.response.data);
      });
  });

  return data;
};

export const register = async (params: any) => {
  const data: any = await new Promise((resolve) => {
    axios.post(`${process.env.REACT_APP_ENDPOINT}/signup`,
      params,
      { headers: { Accept: 'application/json' } })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        resolve(error.response.data);
      });
  });

  return data;
};

export const logout = async () => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const data: any = await new Promise((resolve, reject) => {
    axios.post(`${process.env.REACT_APP_ENDPOINT}/logout`,
      {},
      { headers: { Authorization: authedPayload?.token } })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error.response.message);
      });
  });

  return data;
};
