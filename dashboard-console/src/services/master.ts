import axios from 'axios';
import { Master } from 'src/types/master';
import { getItem } from 'src/utils/encrypt';

export const getMasters = async () => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const masters: Master[] = await new Promise((resolve, reject) => {
    axios.get(`${process.env.REACT_APP_ENDPOINT}/masters`,
      { headers: { Authorization: authedPayload?.token } })
      .then((response) => {
        resolve(response.data.masters);
      })
      .catch((error) => {
        reject(error.response.message);
      });
  });

  return masters;
};

export const getMaster = async (id: string) => {
  const authedPayload: any = getItem(process.env.REACT_APP_NAME);
  const master: Master = await new Promise((resolve, reject) => {
    axios.get(`${process.env.REACT_APP_ENDPOINT}/masters/${id}`,
      { headers: { Authorization: authedPayload?.token } })
      .then((response) => {
        resolve(response.data.master);
      })
      .catch((error) => {
        reject(error.response.message);
      });
  });

  return master;
};
