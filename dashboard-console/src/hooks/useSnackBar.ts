import { useContext } from 'react';
import SnackbarContext, { SnackbarContextValue } from 'src/contexts/SnackBarContext';

const useSnackBar = (): SnackbarContextValue => useContext(SnackbarContext);

export default useSnackBar;
