import { Suspense, lazy } from 'react';
import type { PartialRouteObject } from 'react-router';
import LoadingScreen from './components/LoadingScreen';

const Loadable = (Component) => (props) => (
  <Suspense fallback={<LoadingScreen />}>
    <Component {...props} />
  </Suspense>
);

const DashboardLayout = Loadable(lazy(() => import('./components/dashboard/DashboardLayout')));
const Masters = Loadable(lazy(() => import('./pages/master/masters')));
const Master = Loadable(lazy(() => import('./pages/master/master')));
const Users = Loadable(lazy(() => import('./pages/user/users')));
const Profile = Loadable(lazy(() => import('./pages/user/profile')));

const Login = Loadable(lazy(() => import('./pages/login')));
const Register = Loadable(lazy(() => import('./pages/register')));
const NotFound = Loadable(lazy(() => import('./pages/notfound')));

const routes: PartialRouteObject[] = [
  {
    path: '/',
    element: <DashboardLayout />,
    children: [
      {
        path: 'master',
        element: <Masters />
      },
      {
        path: 'master/:id',
        element: <Master />
      },
      {
        path: 'user',
        element: <Users />
      },
      {
        path: 'me',
        element: <Profile />
      }
    ]
  },
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/register',
    element: <Register />,
  },
  {
    path: '*',
    element: <NotFound />
  },
];

export default routes;
