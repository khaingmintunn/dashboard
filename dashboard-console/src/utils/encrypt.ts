import crypto from 'crypto-js';

export const setItem = (name: string, item: any) => {
  const encoded = crypto.AES.encrypt(
    JSON.stringify(item),
    process.env.REACT_APP_SALT_KEY
  ).toString();
  localStorage.setItem(name, encoded);
};

export const getItem = (name: string) => {
  try {
    let res = '';
    const item = localStorage.getItem(name);
    if (item) {
      const decrypted = crypto.AES.decrypt(
        item,
        process.env.REACT_APP_SALT_KEY
      ).toString(crypto.enc.Utf8);
      res = decrypted ? JSON.parse(decrypted) : res;
    }
    return res;
  } catch {
    return null;
  }
};
