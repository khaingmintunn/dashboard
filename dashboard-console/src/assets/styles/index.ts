import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles(() => ({
  headerBox: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '2rem 0'
  },
  timeText: {
    fontSize: '10px'
  },
  cardHeaderBox: {
    display: 'flex',
    position: 'sticky',
    top: '0',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '1rem 1.5rem',
    backgroundColor: '#404040'
  },
  whiteButton: {
    backgroundColor: '#FFFFFF',
    color: '#000000',
    fontWeight: 'normal',
    marginRight: '2rem',
    borderRadius: '5px !important',
    '&:hover': {
      backgroundColor: '#FFFFFF',
      color: '#000000'
    },
  },
  grayButton: {
    backgroundColor: '#777777',
    color: '#FFFFFF',
    fontWeight: 'normal',
    marginRight: '2rem',
    '&:hover': {
      backgroundColor: '#777777',
      color: '#FFFFFF'
    },
  },
  uploadCardHeaderBox: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#DDDDDD'
  },
  uploadCardHeaderText: {
    padding: '0.5rem 1rem'
  },
  uploadCardHeaderAddText: {
    padding: '0.5rem 1rem',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#CCCCCC',
      color: '#FFFFFF'
    },
    '&:active': {
      backgroundColor: '#BBBBBB',
      color: '#FFFFFF',
      animation: 'ripple 600ms linear'
    },
  },
  textBoxHeader: {
    fontSize: '14px',
    marginTop: '2rem',
    color: '#6b778c',
  },
  paper: {
    paddingBottom: '16px',
    marginTop: '16px',
    marginBottom: '16px',
    borderRadius: '0 !important'
  },
  formField: {
    margin: '20px 0'
  },
  formControl: {
    width: '45%',
    margin: '20px 0',
    borderRadius: '0 !important'
  },
  textHeader: {
    fontSize: '14px',
    marginTop: '2rem',
    marginBottom: '0.5rem',
    color: '#6b778c'
  },
  textField: {
    fontWeight: 'normal',
    fontSize: '14px',
  },
  accordinExpend: {
    margin: '0 !important',
    borderRadius: '0 !important'
  },
  accrodinIconFront: {
    flexDirection: 'row-reverse'
  },
  accordinContent: {
    justifyContent: 'space-between'
  },
  formSubTitle: {
    fontSize: '18px',
    fontWeight: 'bold',
    marginTop: '35px',
    marginBottom: '35px',
  },
  editor: {
    border: '1px solid #DDDDDD',
    minHeight: '10rem'
  },
  multiSelect: {
    margin: '25px 0'
  },
  singleFileContainer: {
    padding: 20
  },
  multiFileContainer: {
    height: 150,
    margin: 16,
  },
  singleImg: {
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    p: 2
  },
  multiImg: {
    textAlign: 'center',
    justifyContent: 'center',
  },
  pdfBlock: {
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    p: 2
  },
  fileRemoveBtn: {
    position: 'absolute',
    top: 0,
    right: 0
  },
  next: {
    padding: '8px',
    textDecoration: 'none',
    textAlign: 'center'
  },
}));
