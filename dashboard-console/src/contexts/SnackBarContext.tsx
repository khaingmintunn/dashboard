import { Alert, Snackbar } from '@material-ui/core';
import { createContext, ReactNode, useState } from 'react';

export interface SnackData {
  open: boolean;
  message: string;
  severity: 'error' | 'success' | 'warning' | 'info';
}

const defaultSnackData: SnackData = {
  open: false,
  message: '',
  severity: 'info'
};

export interface SnackbarContextValue {
  snack: SnackData;
  setSnack: (data: SnackData) => void;
}

interface SnackbarProviderProps {
  children?: ReactNode;
}

const SnackbarContext = createContext<SnackbarContextValue>({
  snack: defaultSnackData,
  setSnack: () => { }
});

export const SnackbarProvider = (props: SnackbarProviderProps) => {
  const { children } = props;
  const vertical = 'top';
  const horizontal = 'center';
  const [snack, setSnack] = useState<SnackData>(defaultSnackData);
  const handleClose = () => setSnack({ ...snack, open: false, });
  return (
    <SnackbarContext.Provider value={{ snack, setSnack }}>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={snack.open}
        onClose={handleClose}
        autoHideDuration={5000}
        key={vertical + horizontal}
      >
        <Alert
          onClose={handleClose}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar>
      {children}
    </SnackbarContext.Provider>
  );
};

export default SnackbarContext;
