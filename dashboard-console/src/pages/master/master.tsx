import { Box, Button, Container, Paper, TextField, Typography } from '@material-ui/core';
import { Formik } from 'formik';
import { FC, useEffect, useState } from 'react';
import { useStyles } from 'src/assets/styles';
import Loader from 'src/components/atoms/Loader';
import { ConnectedFocusError } from 'focus-formik-error';
import * as Yup from 'yup';
import { matchPath, useLocation, useNavigate } from 'react-router-dom';
import { Master } from 'src/types/master';
import { getMaster } from 'src/services/master';

const MasterComponent: FC = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();
  const match = matchPath({
    path: 'master/:id'
  }, location.pathname);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [master, setMaster] = useState<Master>({
    id: 0,
    external_id: 0,
    name: '',
    is_active: 0,
    list_order: 0,
    created: '',
    modified: '',
  });

  const fetchMaster = async () => {
    const data: Master = await getMaster(match.params.id);
    setMaster(data);
  };

  const gotoList = async () => {
    navigate('/master');
  };

  useEffect(() => {
    fetchMaster();
    setIsLoading(false);
  }, []);

  return (
    <Container>
      <Box
        className={classes.headerBox}
      >
        <Typography variant="h5">Master Information</Typography>
      </Box>
      {isLoading ? (
        <Box sx={{ mt: 15 }}>
          <Loader />
        </Box>
      ) : (
        <Paper
          className={classes.paper}
        >
          <Box>
            <Formik
              initialValues={{
                id: master.id,
                external_id: master.external_id,
                name: master.name,
                is_active: master.is_active,
                list_order: master.list_order,
                created: master.created,
                modified: master.modified,
              }}
              validationSchema={
                Yup
                  .object()
                  .shape({})
              }
              enableReinitialize
              onSubmit={() => { }}
            >
              {({
                touched,
                handleChange,
                handleSubmit,
                errors,
                values
              }): JSX.Element => (
                <form
                  onSubmit={handleSubmit}
                >
                  <ConnectedFocusError />
                  <Box
                    className={classes.cardHeaderBox}
                    sx={{ zIndex: 2 }}
                  >
                    <Box
                      sx={{
                        display: 'flex'
                      }}
                    >
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        sx={{
                          fontWeight: 'fontWeightMedium',
                          borderRadius: '5px !important'
                        }}
                        onClick={() => gotoList()}
                      >
                        Master List
                      </Button>
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      m: 3,
                      zIndex: 1
                    }}
                  >
                    <TextField
                      error={Boolean(touched.id && errors.id)}
                      helperText={touched.id && errors.id}
                      label="Id"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="id"
                      value={values.id}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.external_id && errors.external_id)}
                      helperText={touched.external_id && errors.external_id}
                      label="External Id"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="external_id"
                      value={values.external_id}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      helperText={touched.name && errors.name}
                      label="name"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Name"
                      value={values.name}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.is_active && errors.is_active)}
                      helperText={touched.is_active && errors.is_active}
                      label="Active"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="is_active"
                      value={values.is_active}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.list_order && errors.list_order)}
                      helperText={touched.list_order && errors.list_order}
                      label="list_order"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="List Order"
                      value={values.list_order}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.created && errors.created)}
                      helperText={touched.created && errors.created}
                      label="created"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Created At"
                      value={values.created}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.modified && errors.modified)}
                      helperText={touched.modified && errors.modified}
                      label="modified"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Updated At"
                      value={values.modified}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />
                  </Box>
                </form>
              )}
            </Formik>
          </Box>
        </Paper>
      )}
    </Container>
  );
};

export default MasterComponent;
