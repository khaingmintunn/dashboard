import { Box, Container, Typography } from '@material-ui/core';
import { FC, useEffect, useState } from 'react';
import Loader from 'src/components/atoms/Loader';
import TableComponent from 'src/components/TableComponent';
import { getMasters } from 'src/services/master';
import { Master } from 'src/types/master';

const tableColumns = [
  { colName: 'External ID', colVal: 'external_id' },
  { colName: 'Name', colVal: 'name' },
  { colName: 'Active', colVal: 'is_active' },
  { colName: 'Order', colVal: 'list_order' },
  { colName: 'Created', colVal: 'created' },
  { colName: 'Modified', colVal: 'modified' },
  { colName: '', colVal: 'id', isEdit: true },
];
const MasterListComponent: FC = () => {
  const [masters, setMasters] = useState<Master[]>([]);
  const [isLoading, setIsLoaing] = useState<boolean>(true);

  const fetchBooks = async () => {
    const results: Master[] = await getMasters();
    setMasters(results);
  };

  useEffect(() => {
    const exec = async () => {
      await fetchBooks();
      setIsLoaing(false);
    };
    exec();
  }, []);

  return (
    <Container>
      <Box
        sx={{
          mt: 3
        }}
      >
        <Typography variant="h5">Master List</Typography>
      </Box>
      <Box>
        {isLoading ? (
          <Loader />
        ) : (
          <Box
            sx={{
              mt: 3,
              mb: 3
            }}
          >
            {masters.length > 0
              ? (
                <>
                  <TableComponent
                    columns={tableColumns}
                    tablebody={masters}
                    isSorted
                    isPagination
                    sortedDirection={[{ name: tableColumns[0].colVal, dir: 'desc' }, { name: tableColumns[1].colVal, dir: 'desc' }, { name: tableColumns[3].colVal, dir: 'desc' }]}
                  />
                </>
              )
              : (
                <>
                  <Typography>
                    No Data
                  </Typography>
                </>
              )}
          </Box>
        )}
      </Box>
    </Container>
  );
};

export default MasterListComponent;
