import { Box, Container, Typography } from '@material-ui/core';
import { FC, useEffect, useState } from 'react';
import Loader from 'src/components/atoms/Loader';
import TableComponent from 'src/components/TableComponent';
import { getUser } from 'src/services/user';
import { User } from 'src/types/user';

const tableColumns = [
  { colName: 'Name', colVal: 'name' },
  { colName: 'email', colVal: 'email' },
  { colName: 'Verified At', colVal: 'email_verified_at' },
  { colName: 'Created', colVal: 'created_at' },
  { colName: 'Modified', colVal: 'updated_at' },
];
const UserListComponent: FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [isLoading, setIsLoaing] = useState<boolean>(true);

  const fetchBooks = async () => {
    const results: User[] = await getUser();
    setUsers(results);
  };

  useEffect(() => {
    const exec = async () => {
      await fetchBooks();
      setIsLoaing(false);
    };
    exec();
  }, []);

  return (
    <Container>
      <Box
        sx={{
          mt: 3
        }}
      >
        <Typography variant="h5">User List</Typography>
      </Box>
      <Box>
        {isLoading ? (
          <Loader />
        ) : (
          <Box
            sx={{
              mt: 3,
              mb: 3
            }}
          >
            {users.length > 0
              ? (
                <>
                  <TableComponent
                    columns={tableColumns}
                    tablebody={users}
                    isSorted
                    isPagination
                    sortedDirection={[{ name: tableColumns[0].colVal, dir: 'desc' }]}
                  />
                </>
              )
              : (
                <>
                  <Typography>
                    No Data
                  </Typography>
                </>
              )}
          </Box>
        )}
      </Box>
    </Container>
  );
};

export default UserListComponent;
