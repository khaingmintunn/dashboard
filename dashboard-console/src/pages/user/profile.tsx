import { Box, Button, CircularProgress, Container, Paper, TextField, Typography } from '@material-ui/core';
import { Formik } from 'formik';
import { FC, useEffect, useState } from 'react';
import { useStyles } from 'src/assets/styles';
import Loader from 'src/components/atoms/Loader';
import { getProfile, updateProfile } from 'src/services/user';
import { ConnectedFocusError } from 'focus-formik-error';
import { User } from 'src/types/user';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import useSnackBar from 'src/hooks/useSnackBar';

const ProfileComponent: FC = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { setSnack } = useSnackBar();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isSubmit, setIsSubmit] = useState<boolean>(false);
  const [user, setUser] = useState<User>({
    id: '',
    email: '',
    name: '',
    password: '',
    email_verified_at: '',
    created_at: '',
    updated_at: '',
  });

  const fetchProfile = async () => {
    const data: User = await getProfile();
    data.password = '';
    data.email_verified_at = '';
    setUser(data);
  };

  const gotoList = async () => {
    navigate('/user');
  };

  const profileUpdate = async (params: User) => {
    setIsSubmit(true);
    const payload: any = { name: params.name };
    if (params.email !== user.email) payload.email = params.email;
    if (params.password) payload.password = params.password;
    const result: any = await updateProfile(payload);
    if (result.id) {
      setSnack({ open: true, message: 'Profile updated successfully.', severity: 'success' });
      navigate('/user');
    } else {
      setIsSubmit(false);
      setSnack({ open: true, message: result.message, severity: 'error' });
    }
  };

  useEffect(() => {
    fetchProfile();
    setIsLoading(false);
  }, []);

  return (
    <Container>
      <Box
        className={classes.headerBox}
      >
        <Typography variant="h5">Account Information</Typography>
      </Box>
      {isLoading ? (
        <Box sx={{ mt: 15 }}>
          <Loader />
        </Box>
      ) : (
        <Paper
          className={classes.paper}
        >
          <Box>
            <Formik
              initialValues={{
                id: user.id,
                email: user.email,
                name: user.name,
                password: user.password,
                email_verified_at: user.email_verified_at,
                created_at: user.created_at,
                updated_at: user.updated_at,
              }}
              validationSchema={
                Yup
                  .object()
                  .shape({
                    name: Yup.string().min(4).max(30),
                    email: Yup.string().email(),
                    password: Yup.string().min(8).max(30),
                  })
              }
              enableReinitialize
              onSubmit={async (values): Promise<void> => {
                try {
                  await profileUpdate(values);
                } catch (err) {
                  console.error(err);
                }
              }}
            >
              {({
                touched,
                handleChange,
                handleSubmit,
                errors,
                values
              }): JSX.Element => (
                <form
                  onSubmit={handleSubmit}
                >
                  <ConnectedFocusError />
                  <Box
                    className={classes.cardHeaderBox}
                    sx={{ zIndex: 2 }}
                  >
                    <Box>
                      <Button
                        variant="text"
                        color="primary"
                        type="button"
                        onClick={() => gotoList()}
                        className={classes.whiteButton}
                      >
                        Cancel
                      </Button>
                    </Box>
                    <Box
                      sx={{
                        display: 'flex'
                      }}
                    >
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        sx={{
                          fontWeight: 'fontWeightMedium',
                          borderRadius: '5px !important'
                        }}
                        disabled={isSubmit}
                      >
                        {isSubmit
                          && (
                            <CircularProgress
                              size={12}
                              sx={{ mr: 2 }}
                            />
                          )}
                        Update
                      </Button>
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      m: 3,
                      zIndex: 1
                    }}
                  >
                    <TextField
                      error={Boolean(touched.id && errors.id)}
                      helperText={touched.id && errors.id}
                      label="Id"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="id"
                      value={values.id}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                      disabled
                    />

                    <TextField
                      error={Boolean(touched.name && errors.name)}
                      helperText={touched.name && errors.name}
                      label="Name"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="name"
                      value={values.name}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.email && errors.email)}
                      helperText={touched.email && errors.email}
                      label="Email"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="email"
                      value={values.email}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.password && errors.password)}
                      helperText={touched.password && errors.password}
                      label="Password"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="password"
                      value={values.password}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                    />

                    <TextField
                      error={Boolean(touched.email_verified_at && errors.email_verified_at)}
                      helperText={touched.email_verified_at && errors.email_verified_at}
                      label="email_verified_at"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Email Verified At"
                      value={values.email_verified_at}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                      disabled
                    />

                    <TextField
                      error={Boolean(touched.created_at && errors.created_at)}
                      helperText={touched.created_at && errors.created_at}
                      label="created_at"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Created At"
                      value={values.created_at}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                      disabled
                    />

                    <TextField
                      error={Boolean(touched.updated_at && errors.updated_at)}
                      helperText={touched.updated_at && errors.updated_at}
                      label="updated_at"
                      fullWidth
                      InputProps={{
                        classes: {
                          input: classes.textField,
                        },
                      }}
                      InputLabelProps={{
                        className: classes.textField
                      }}
                      margin="normal"
                      name="Updated At"
                      value={values.updated_at}
                      variant="standard"
                      onChange={handleChange}
                      type="text"
                      disabled
                    />
                  </Box>
                </form>
              )}
            </Formik>
          </Box>
        </Paper>
      )}
    </Container>
  );
};

export default ProfileComponent;
