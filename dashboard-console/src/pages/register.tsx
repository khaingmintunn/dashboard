import { Box, Button, Container, FormHelperText, Link, makeStyles, TextField, Typography } from '@material-ui/core';
import { Formik } from 'formik';
import { FC, useCallback } from 'react';
import * as Yup from 'yup';
import useMounted from '../hooks/useMounted';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { register } from 'src/services/auth';

const useStyles = makeStyles(() => ({
  container: {
    margin: 'auto',
    width: '40%',
    padding: '9% 0',
  }
}));

const form = {
  initialValue: {
    name: '',
    email: '',
    password: '',
    submit: null
  },
  validator: Yup
    .object()
    .shape({
      name: Yup
        .string()
        .min(4)
        .max(30)
        .required('Name is required.'),
      email: Yup
        .string()
        .email('Email must be email format.')
        .max(255)
        .required('Email is required.'),
      password: Yup
        .string()
        .min(8)
        .max(30)
        .required('Password is required.')
    })
};

const RegisterComponent: FC = () => {
  const classes = useStyles();
  const mounted = useMounted();
  const navigate = useNavigate();

  const onSubmit = useCallback(async (values, { setErrors, setStatus, setSubmitting }) => {
    try {
      const { name, email, password } = values;
      const result = await register({ name, email, password });
      if (!result.success) {
        setStatus({ success: false });
        setErrors({ submit: result.message });
        setSubmitting(false);
      }
      if (mounted.current && result.success) {
        navigate('/login');
        setStatus({ success: true });
        setSubmitting(false);
      }
    } catch (err) {
      if (mounted.current) {
        setStatus({ success: false });
        setErrors({ submit: err.message });
        setSubmitting(false);
      }
    }
  }, []);
  return (
    <Container className={classes.container}>
      <Box sx={{ mb: 2 }}>
        <Typography
          sx={{
            color: '#0000ff',
            fontSize: 30,
            textAlign: 'center',
            fontWeight: 'bold',
            fontFamily: 'sans-serif',
            fontVariant: 'small-caps',
            textShadow: '2px 2px #99aa99'
          }}
        >
          Dashboard
        </Typography>
      </Box>
      <Typography
        sx={{
          color: '#999999',
          fontSize: 15,
          fontFamily: 'sans-serif',
        }}
      >
        Register New Dashboard Account
      </Typography>
      <Formik
        initialValues={form.initialValue}
        validationSchema={form.validator}
        onSubmit={onSubmit}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          touched,
          values
        }): JSX.Element => (
          <form
            noValidate
            onSubmit={handleSubmit}
          >
            <TextField
              error={Boolean(touched.name && errors.name)}
              fullWidth
              helperText={touched.name && errors.name}
              label="Name"
              margin="normal"
              name="name"
              onBlur={handleBlur}
              onChange={handleChange}
              type="text"
              value={values.name}
              variant="outlined"
            />
            <TextField
              error={Boolean(touched.email && errors.email)}
              fullWidth
              helperText={touched.email && errors.email}
              label="Email"
              margin="normal"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              type="email"
              value={values.email}
              variant="outlined"
            />
            <TextField
              error={Boolean(touched.password && errors.password)}
              fullWidth
              helperText={touched.password && errors.password}
              label="Password"
              margin="normal"
              name="password"
              onBlur={handleBlur}
              onChange={handleChange}
              type="password"
              value={values.password}
              variant="outlined"
            />
            {errors.submit && (
              <Box sx={{ mt: 3 }}>
                <FormHelperText error>
                  {errors.submit}
                </FormHelperText>
              </Box>
            )}
            <Box sx={{ mt: 2, textAlign: 'center' }}>
              <Button
                color="primary"
                disabled={isSubmitting}
                size="large"
                type="submit"
                variant="contained"
                sx={{ mt: 3, pt: 1, pr: 4, pb: 1, pl: 4 }}
              >
                Register
              </Button>
            </Box>
          </form>
        )}
      </Formik>
      <Box sx={{ textAlign: 'center', mt: 4 }}>
        <Link
          color="textSecondary"
          component={RouterLink}
          sx={{ mt: 1 }}
          to="/login"
          variant="body2"
        >
          Already exist? Login
        </Link>
      </Box>
    </Container>
  );
};

export default RegisterComponent;
